# api

Il faut installer, dans l'ordre :
- docker : <https://docs.docker.com/install/linux/docker-ce/ubuntu/>
- docker-compose : <https://docs.docker.com/compose/install/>

Une fois l'environnement installé, placez-vous à la racine du projet et utilisez la commande suivante pour lancer l'environnement :
```bash
docker-compose up
```

Si ça marche pas, essayer en sudo.
En cas d'erreur 
```bash
ERROR : Get https://registry.gitlab.com/v2/paukamama/osrm/manifests/latest: denied: access forbidden
```
Faire :
```bash
docker login registry.gitlab.com
```
Entrer Username et Password de gitlab.


Pour arrêter l'environnement :
```bash
docker-compose stop
```

Pour nettoyer l'environnement :
```bash
docker-compose down
```

**Attention, Docker ne fonctionne pas de la même manière sur Windows et sur Linux! Utilisez la version Linux pour ne pas avoir de problèmes.**