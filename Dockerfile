FROM python:3.8

ENV PYTHONUNBUFFERED 1
ENV PRODUCTION 1

RUN mkdir /web

WORKDIR /web

ADD requirements.txt /web/

RUN apt-get update && \
 apt-get install -y libspatialindex-dev libgdal-dev libgeos-dev

RUN python3 -m pip install -r requirements.txt --no-cache-dir

ADD . /web/

CMD /bin/bash /web/docker-entrypoint.sh

EXPOSE 8000
