#!/bin/bash

python3 manage.py migrate --no-input
# python3 manage.py collectstatic --no-input

exec gunicorn api.wsgi:application -b 0.0.0.0:8000 --workers 2 --reload
