import json
from json.decoder import JSONObject
from random import uniform
from statistics import mean
from typing import List, Dict

import requests
from django.core.cache import cache
from django.http import JsonResponse
from geopy.distance import distance

from api import settings
from route.utils import PathOrder


def get_cumulated_pollution_index_for_path(subpaths: List[Dict], length: float) -> float:
    """ Permet de calculer l'indice de pollution cumulé pour un tronçon
    :param subpaths: liste de tronçons composant le chemin sous le format {
            "coordinates": [latitude, longitude],
            "pollution": entier entre 0 et 10,
            "length": float en km
        }
    :param length: longueur du chemin, pour normalisation
    :return la somme des coefficients de pollution de chaque tronçon pondérée par la longueur de chaque tronçon, normalisée à 10
    """
    return round(sum([s["pollution"] * s["length"] for s in subpaths]) / length)


def get_mean_point(start: List[float], end: List[float])-> List[float]:
    """ Permet de calculer le centre d'un tronçon
    :param start: coordonnées [lat,lng] du point de début
    :param end: coordonnées [lat,lng] du point de fin
    :return les coordonnées [lat,lng] du point central
    """
    mean_point_lat = round(mean([start[0], end[0]]), 6)
    mean_point_long = round(mean([start[1], end[1]]), 6)
    return [mean_point_lat, mean_point_long]


def get_aqi_from_api(subpath_center: List[float]) -> int:
    """ Permet de récupérer un indice de pollution de l'API Breezometer
    :param subpath_center: coordonnées du point central du tronçon
    :return la valeur de l'indice de pollution (/100)
    """
    query_url = f"https://api.breezometer.com/air-quality/v2/current-conditions?lat={subpath_center[0]}&lon={subpath_center[1]}&key={settings.BREZOMETER_API_KEY}"
    response = requests.get(query_url)
    content_json = json.loads(response.content)
    if content_json['error'] is None:
        aqi = content_json['data']['indexes']['baqi']['aqi']
        return 100 - aqi
    return round(uniform(0, 100))


def get_pollution_index_for_subpath(subpath_start: List[float], subpath_end: List[float]) -> int:
    """ Permet de calculer l'indice de pollution pour un tronçon

    :param subpath_start: coordonnées de début du tronçon, sous le format [latitude, longitude]
    :param subpath_end: coordonnées de fin du tronçon, sous le format [latitude, longitude]
    :return l'indice de pollution entre 0 et 100
    """
    if settings.RANDOM_INDEX:
        return round(uniform(0, 100))

    mean_point = get_mean_point(subpath_start, subpath_end)
    return get_aqi_from_api(mean_point)


def set_pollution_indices_for_path(path, cached_radius_m: int = 500):
    """ Permet de paramétrer les indices de pollution

    La fonction essaye de récupérer un indice de pollution dans le cache (toute valeur dans un rayon donné), ou le
    recalcule
    :param path: le chemin donné
    :param cached_radius_m: le rayon dans lequel on cherche la valeur sauvegardée
    """
    cached_coordinates = cache.get('cached_coordinates', default=[])
    empty_coordinates = []
    for i, s in enumerate(path['subpaths']):
        subpath_start = s['coordinates'][0]
        subpath_end = s['coordinates'][1]
        subpath_center = get_mean_point(subpath_start, subpath_end)
        for j, (lat, lng) in enumerate(cached_coordinates):
            if distance((subpath_center[0], subpath_center[1]), (lat, lng)).m < cached_radius_m:
                pollution = cache.get(f"({lat};{lng})", None)
                if pollution is not None:
                    s['pollution'] = pollution
                    break
                else:
                    empty_coordinates.append((lat, lng))

        if s['pollution'] is None:
            pollution = get_pollution_index_for_subpath(subpath_start, subpath_end)
            s['pollution'] = pollution
            cache.set(f"({subpath_center[0]};{subpath_center[1]})", pollution, 3600)
            cached_coordinates.append((subpath_center[0], subpath_center[1]))

    for (lat, lng) in empty_coordinates:
        cached_coordinates.remove((lat, lng))
    cache.set('cached_coordinates', cached_coordinates)


def get_subpath_length(subpath_start: List[float], subpath_end: List[float]) -> float:
    """ Permet de calculer la longueur d'un tronçon

    :param subpath_start: coordonnées de début du tronçon, sous le format [latitude, longitude]
    :param subpath_end: coordonnées de fin du tronçon, sous le format [latitude, longitude]
    :return la longueur du tronçon en kilomètres
    """
    query_url = f"{settings.OSRM_BACKEND_ROOT}/route/v1/bike/{subpath_start[1]},{subpath_start[0]};{subpath_end[1]},{subpath_end[0]}?geometries=geojson&continue_straight=true"
    response = requests.get(query_url)
    content_json = json.loads(response.content)
    return round(content_json['routes'][0]["distance"] / 1000, 2)


def break_down_subpath(subpath_start: List[float], subpath_end: List[float], max_subpath_length=1) -> List[Dict]:
    """ Permet de subdiviser un tronçon s'il est trop long

    :param subpath_start: coordonnées de début du tronçon, sous le format [latitude, longitude]
    :param subpath_end: coordonnées de fin du tronçon, sous le format [latitude, longitude]
    :param max_subpath_length: longueur maximale du tronçon (0.5km par défaut)
    :return une liste de tronçons, correspondant à la subdivision du tronçon initial, sous le format {
            "coordinates": [latitude, longitude],
            "pollution": entier entre 0 et 10,
            "length": float en km
        }
    """
    subpath_length = get_subpath_length(subpath_start=subpath_start, subpath_end=subpath_end)

    if subpath_length < max_subpath_length:
        return [{
            "coordinates": [subpath_start, subpath_end],
            "pollution": None,
            "length": subpath_length
        }]

    subpaths = []
    mean_point = get_mean_point(subpath_start, subpath_end)
    subpaths.extend(break_down_subpath(subpath_start=subpath_start, subpath_end=mean_point))
    subpaths.extend(break_down_subpath(subpath_start=mean_point, subpath_end=subpath_end))

    return subpaths


def get_subpaths_from_coordinates(coordinates: List[List[float]]) -> List[Dict]:
    """ Permet de créer les tronçons à partir de la liste de coordonnées sur le chemin

    :param coordinates: liste de coordonnées sous le format [latitude, longitude]
    :return une liste de tronçons sous le format {
            "coordinates": [latitude, longitude],
            "pollution": entier entre 0 et 10,
            "length": float en km
        }
    """
    subpaths = []
    last_item_index = len(coordinates) - 1
    for i, coordinate in enumerate(coordinates):
        if i != last_item_index:
            subpaths.extend(break_down_subpath(subpath_start=coordinate, subpath_end=coordinates[i + 1]))

    return subpaths


def convert_coordinate_list_to_json_object(coordinate: List[float]) -> JSONObject:
    """ Permet de convertir une coordonnée du format accepté par react-leaflet au format accepté par react-native-maps

    :param coordinate: coordonées au format [latitude, longitude]
    :return: objet JSON représentant les coordonnées
    """
    return {
        "latitude": coordinate[0],
        "longitude": coordinate[1]
    }


def route(request, src_lat: float, src_long: float, dst_lat: float, dst_long: float,
          order_by: PathOrder = PathOrder.CLEANEST, mobile: bool = False, max_nb_chemins: int = 2) -> JsonResponse:
    print(f"Requested route between [{src_lat}, {src_long}] and [{dst_lat}, {dst_long}]")
    src_lat, src_long, dst_lat, dst_long = float(src_lat), float(src_long), float(dst_lat), float(dst_long)

    query_url = f"{settings.OSRM_BACKEND_ROOT}/route/v1/bike/{src_long},{src_lat};{dst_long},{dst_lat}?alternatives={max_nb_chemins}&geometries=geojson"
    response = requests.get(query_url)
    content_json = json.loads(response.content)

    paths = []
    for path in content_json['routes']:
        coordinates = [[c[1], c[0]] for c in path['geometry']['coordinates']]
        paths.append({
            'subpaths': get_subpaths_from_coordinates(coordinates),
            'distance': round(path['distance'] / 1000, 2),
            'duration': round(path['duration'] / 60),
            'pollution': None
        })

    for path in paths:
        set_pollution_indices_for_path(path)
        path['pollution'] = get_cumulated_pollution_index_for_path(path['subpaths'], path['distance'])
        print()
        if mobile:
            for s in path['subpaths']:
                s['coordinates'] = [convert_coordinate_list_to_json_object(c) for c in s["coordinates"]]

    paths.sort(key=lambda p: p[order_by.value])

    return JsonResponse({"paths": paths})
