from django.urls import path

from route import views
from route.utils import PathOrder

urlpatterns = [
    path("src/<src_lat>/<src_long>/dst/<dst_lat>/<dst_long>/", views.route),
    path("src/<src_lat>/<src_long>/dst/<dst_lat>/<dst_long>/mobile/", views.route, {"mobile": True}),
    path("fastest/src/<src_lat>/<src_long>/dst/<dst_lat>/<dst_long>/mobile/", views.route,
         {"mobile": True, "order_by": PathOrder.FASTEST}),
    path("shortest/src/<src_lat>/<src_long>/dst/<dst_lat>/<dst_long>/mobile/", views.route,
         {"mobile": True, "order_by": PathOrder.SHORTEST}),
    path("cleanest/src/<src_lat>/<src_long>/dst/<dst_lat>/<dst_long>/mobile/", views.route,
         {"mobile": True, "order_by": PathOrder.CLEANEST})
]
