import sys
from enum import Enum
import osmnx
from django.core.cache import cache


class PathOrder(Enum):
    FASTEST = 'duration'
    SHORTEST = 'distance'
    CLEANEST = 'pollution'


def get_graph():
    graph = cache.get("graph")
    if not graph:
        print("Graph not available in cache, building it now. Please wait!", file=sys.stderr)
        graph = osmnx.graph_from_place("Lyon, France")
        cache.set("graph", graph, timeout=None)
        print("Graph built successfully.", file=sys.stderr)

    return graph
